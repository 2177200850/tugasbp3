package com.devour.tugascrud;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.devour.tugascrud.api.ApiRequestBiodata;
import com.devour.tugascrud.api.Retroserver;
import com.devour.tugascrud.model.ResponsModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    EditText nama,nim,prodi,nohp;
    Button btnsave, btntampil,btnupdate,btndelete;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nama = (EditText) findViewById(R.id.edt_name);
        nim = (EditText) findViewById(R.id.edt_nim);
        prodi = (EditText) findViewById(R.id.edt_prodi);
        nohp = (EditText) findViewById(R.id.edt_nohp);
        btnsave = (Button) findViewById(R.id.btn_save);
        btntampil = (Button) findViewById(R.id.btn_tampildata);
        btnupdate = (Button) findViewById(R.id.btn_updatedata);
        btndelete = (Button) findViewById(R.id.btn_delete);

        Intent data = getIntent();
        final String iddata = data.getStringExtra("id");
        if (iddata  != null) {

            btnsave.setVisibility(View.GONE);
            btntampil.setVisibility(View.GONE);
            btnupdate.setVisibility(View.VISIBLE);
            btndelete.setVisibility(View.VISIBLE);


            nama.setText(data.getStringExtra("nama"));
            nim.setText(data.getStringExtra("nim"));
            prodi.setText(data.getStringExtra("prodi"));
            nohp.setText(data.getStringExtra("nohp"));
        }

        pd = new ProgressDialog(this);

        btntampil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent godata = new Intent(MainActivity.this, TampilData.class);
                startActivity(godata);
            }
        });

        btndelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("Loading Hapus ...");
                pd.setCancelable(false);
                pd.show();

                ApiRequestBiodata api = Retroserver.getClient().create(ApiRequestBiodata.class);
                Call<ResponsModel> del  = api.deleteData(iddata);
                del.enqueue(new Callback<ResponsModel>() {
                    @Override
                    public void onResponse(Call<ResponsModel> call, Response<ResponsModel> response) {
                        Log.d("Retro", "onResponse");
                        Toast.makeText(MainActivity.this, response.body().getPesan(),Toast.LENGTH_SHORT).show();
                        pd.hide();
                        Intent gotampil = new Intent(MainActivity.this,TampilData.class);
                        startActivity(gotampil);

                    }

                    @Override
                    public void onFailure(Call<ResponsModel> call, Throwable t) {
                        pd.hide();
                        Log.d("Retro", "onFailure");
                    }
                });
            }
        });

        btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("Updating ...");
                pd.setCancelable(false);
                pd.show();
                ApiRequestBiodata api = Retroserver.getClient().create(ApiRequestBiodata.class);
                Call<ResponsModel> update = api.updateData(iddata,nama.getText().toString(),nim.getText().toString(),prodi.getText().toString(),nohp.getText().toString());
                update.enqueue(new Callback<ResponsModel>() {
                    @Override
                    public void onResponse(Call<ResponsModel> call, Response<ResponsModel> response) {
                        pd.hide();
                        Toast.makeText(MainActivity.this,response.body().getPesan(), Toast.LENGTH_SHORT).show();
                        Log.d("RETRO", "Response");
                    }

                    @Override
                    public void onFailure(Call<ResponsModel> call, Throwable t) {
                        pd.hide();
                        Log.d("RETRO", "Response gagal");
                    }
                });
            }
        });

        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.setMessage("send data ...");
                pd.setCancelable(false);
                pd.show();

                String snama = nama.getText().toString();
                String snim = nim.getText().toString();
                String sprodi = prodi.getText().toString();
                String snohp = nohp.getText().toString();

                ApiRequestBiodata api = Retroserver.getClient().create(ApiRequestBiodata.class);


                Call<ResponsModel> sendbio = api.sendBiodata(snama,snim,sprodi,snohp);
                sendbio.enqueue(new Callback<ResponsModel>() {
                    @Override
                    public void onResponse(Call<ResponsModel> call, Response<ResponsModel> response) {
                        pd.hide();
                        Log.d("RETRO", "response : "+ response.body().toString());
                        String kode = response.body().getKode();

                        if (kode.equals("1"))
                        {
                            Toast.makeText(MainActivity.this, "Data berhasil disimpan", Toast.LENGTH_SHORT).show();
                        }else
                        {
                          Toast.makeText(MainActivity.this, "Data Error ! Tidak berhasil disimpan", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponsModel> call, Throwable t) {
                        pd.hide();
                        Log.d("RETRO", "failure :" + "Gagal Mengirim Request");
                    }
                });
            }
        });
    }
}
